package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.BlankSpell;
import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContractSeal {

    private Map<String, Spell> spells;
    private Spell currSpell;

    public ContractSeal() {
        spells = new HashMap<>();
    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    public void castSpell(String spellName) {
        Spell newSpell = this.spells.get(spellName);
        newSpell.cast();
        this.currSpell = newSpell;
    }

    public void undoSpell() {
        if(currSpell == null) return;

        this.currSpell.undo();
        currSpell = null;
    }

    public Collection<Spell> getSpells() { return spells.values(); }
}

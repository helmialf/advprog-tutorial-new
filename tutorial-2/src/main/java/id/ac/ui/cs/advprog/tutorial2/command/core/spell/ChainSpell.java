package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    protected ArrayList<Spell> spellList;

    public ChainSpell(ArrayList<Spell> spellList){
        this.spellList = spellList;
    }

    @Override
    public void cast(){
        for(Spell spell : spellList){
            spell.cast();
        }
    }

    @Override
    public void undo(){
        int last = spellList.size() - 1;
        for(int i = last; i >= 0; i--){
            spellList.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
